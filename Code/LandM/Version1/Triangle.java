import java.nio.FloatBuffer;

public class Triangle{

  private Vertex v1, v2, v3;

  // create triangle from given vertices
  public Triangle( Vertex v1In, Vertex v2In, Vertex v3In ){
    v1 = v1In;
    v2 = v2In;
    v3 = v3In;
  }

  public String toString(){
    return "v1: " + v1 + "v2: " + v2 + "v3: " + v3;
  }

  // append to the buffers the data for this triangle
  public void toBuffers( 
                FloatBuffer posBuffer, FloatBuffer normalBuffer,
    FloatBuffer emiBuffer, FloatBuffer ambiBuffer, FloatBuffer diffBuffer,
    FloatBuffer specBuffer, FloatBuffer shiniBuffer ){

    v1.toBuffers( posBuffer, normalBuffer,
                  emiBuffer, ambiBuffer, diffBuffer, specBuffer, 
                  shiniBuffer );
    v2.toBuffers( posBuffer, normalBuffer,
                  emiBuffer, ambiBuffer, diffBuffer, specBuffer, 
                  shiniBuffer );
    v3.toBuffers( posBuffer, normalBuffer,
                  emiBuffer, ambiBuffer, diffBuffer, specBuffer, 
                  shiniBuffer );
 
  }// toBuffers

}// Triangle
