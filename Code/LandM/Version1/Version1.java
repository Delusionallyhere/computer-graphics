import java.util.ArrayList;

import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.util.Scanner;
import java.io.File;

public class Version1 extends Basic
{
  public static void main(String[] args)
  {
    Version1 app = new Version1( "Scene with Lighting and Materials", 500, 500, 30, args[0] );
    app.start();
  }// main

  // instance variables 

  private FloatBuffer backColor;
  private Shader v1, f1;
  private int hp1;
  private Program p1;

  private TriSoup first;  // the stationary boxes
  private ArrayList<Thing> list1;  

  private TriSoup second;  // the moveable boxes
  private ArrayList<Thing> list2;

  // handles for uniforms operated by this class
  // (light stuff is done in Light)
  private int projMatrixLoc;  // location of the projection matrix
  private int viewMatrixLoc;  // location of the viewing matrix
  private int eyeLoc;         // location of the eye point vector

  private Mat4 proj, view;  // the projection and viewing matrices
  private Vec4 eye;  // position of eye

  private double azimuth;             // azimuth
  private double altitude;            // tilt from z axis

  private boolean controlEye;  // toggle between controlling eye and light

  // construct basic application with given title, pixel width and height
  // of drawing area, and frames per second
  public Version1( String appTitle, int pw, int ph, int fps, String fileName )
  {
    super( appTitle, pw, ph, (long) ((1.0/fps)*1000000000) );

    try{

      Scanner input;  // use for all input files

      // create desired materials (must do before loading things)
      input = new Scanner( new File( "standard.mat" ) );
      Material.load( input );
      input.close();

      // load scene from data file

      input = new Scanner( new File( fileName ) );

      int num = input.nextInt();  // # of stationary things
      list1 = new ArrayList<Thing>();
      for( int k=0; k<num; k++ ){
        Thing b = new Thing( input );
        list1.add( b );
      }

      // create permanent first soup from immobile things
      first = new TriSoup( list1 );

      num = input.nextInt();  // # of moveable boxes
      list2 = new ArrayList<Thing>();
      for( int k=0; k<num; k++ ){
        Thing b = new Thing( input );
        list2.add( b );
      }

      second = new TriSoup( list2 );  // make initial mobile soup

      input.close();

    }
    catch(Exception e){
      System.out.println("something went wrong with reading files");
      e.printStackTrace();
      System.exit(1);
    }

    // begin controlling eye
    controlEye = true;

    // select initial values for eye

    // look at center of wall
    eye = new Vec4( 65, 0, 6, 1 );
    azimuth = 90;
    altitude = 0;

/*
    // nice view for quick check of scene
    eye = new Vec4( -75, -75, 150, 1 );
    azimuth = 45;
    altitude = -42;
*/

/*
    // way overhead downward view
    eye = new Vec4( 50, 50, 300, 1 );
    azimuth = 90;   
    altitude = -89;  
*/

/*
    // far corner looking back at center
    eye = new Vec4( 110, 110, 0, 1 );
    azimuth = 225;
    altitude = 0;
*/

/*
    // front middle
    eye = new Vec4( 50, -50, 10, 1 );
    azimuth = 90;
    altitude = 0;
*/

    updateView();

  }// constructor

  protected void init()
  {
    String vertexShaderCode = Shader.readFile( "LM1.vert" );
    System.out.println("Vertex shader:\n" + vertexShaderCode + "\n\n" );

    v1 = new Shader( "vertex", vertexShaderCode );

    String fragmentShaderCode = Shader.readFile( "LM1.frag" );

    System.out.println("Fragment shader:\n" + fragmentShaderCode + "\n\n" );

    f1 = new Shader( "fragment", fragmentShaderCode );

    hp1 = GL20.glCreateProgram();
         Util.error("after create program");
         System.out.println("program handle is " + hp1 );

    GL20.glAttachShader( hp1, v1.getHandle() );
         Util.error("after attach vertex shader to program");

    GL20.glAttachShader( hp1, f1.getHandle() );
         Util.error("after attach fragment shader to program");

    GL20.glLinkProgram( hp1 );
         Util.error("after link program" );

    GL20.glUseProgram( hp1 );
         Util.error("after use program");

    // ask program for locations of the uniform variables by name
    projMatrixLoc = GL20.glGetUniformLocation( hp1, "proj" );
         Util.error("after locate proj");
    viewMatrixLoc = GL20.glGetUniformLocation( hp1, "view" );
         Util.error("after locate view");
    eyeLoc = GL20.glGetUniformLocation( hp1, "eye" );
         Util.error("after locate eye");
    
    // set background color to white
    GL11.glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );
 
    // enable depth buffering
    GL11.glEnable( GL11.GL_DEPTH_TEST );
    GL11.glClearDepth( 100.0f );
    GL11.glDepthFunc( GL11.GL_LESS );

    // create permanent proj matrix
    proj = Mat4.frustum( -1, 1, -1, 1, 4, 1000 );

    setupLighting( hp1 );

  }

  // fully set up lighting stuff, need program handle
  // so can locate all the uniforms
  private void setupLighting( int programHandle ){

    Light.init( programHandle, 1, 0, 0, 0, 0 );
    // light0 is the controllable spotlight --- Light class manages,
    //             so we apparently don't store reference to lights anywhere
/*
  public Light( int programHandle, Vec4 p,
                 double azi, double alt,
                 double cutoffAngle, double spotlightExponent,
                 Vec4 amb, Vec4 dif, Vec4 spe ) {
*/

/*  // original
    new Light( programHandle, new Vec4( 65, 0, 6, 1 ),
               90, 0,
               5, 10,
               new Vec4( 0.5, 0.5, 0.5, 1 ),
               new Vec4( 0, 0, 0, 1 ),
               new Vec4( 1, 1, 1, 1 ) );
*/
    new Light( programHandle, new Vec4( 65, 0, 6, 1 ),
               90, 0,
               90, 10,
               new Vec4( 1., 1., 1., 1 ),
               new Vec4( 1, 1, 1, 1 ),
               new Vec4( 1, 1, 1, 1 ) );

/*
    new Light( programHandle, new Vec4( 0, 0, 0, 0 ),
               0, 0,
               0, 0,
               new Vec4( 0, 0, 0, 0 ),
               new Vec4( 0, 0, 0, 0 ),
               new Vec4( 0, 0, 0, 0 ),
               1, 0.5, 0 );

    new Light( programHandle, new Vec4( 0, 0, 0, 0 ),
               0, 0,
               0, 0,
               new Vec4( 0, 0, 0, 0 ),
               new Vec4( 0, 0, 0, 0 ),
               new Vec4( 0, 0, 0, 0 ),
               0, 0, 0 );
*/

  }

  // update view matrix based on eye, angles
  private void updateView(){
    double c = Math.cos( Math.toRadians( azimuth ) );
    double s = Math.sin( Math.toRadians( azimuth ) );
    double c2 = Math.cos( Math.toRadians( altitude ) );
    double s2 = Math.sin( Math.toRadians( altitude ) );
  
    double ex = eye.x[0], ey = eye.x[1], ez = eye.x[2];

    view = Mat4.lookAt( ex, ey, ez,
                        ex + c*c2, ey + s*c2, ez + s2,
                        0, 0, 1 );
    System.out.println("x= " + ex + " y= " + ey + " z= " + ez + " azimuth= " +
                          azimuth + " altitude= " + altitude );
  }

  private static double spaceAmount = 1;
  private static double angleAmount = 1;

  protected void processInputs()
  {
    // process all waiting input events
    while( InputInfo.size() > 0 )
    {
      InputInfo info = InputInfo.get();

      if( info.kind == 'k' && (info.action == GLFW_PRESS || 
                               info.action == GLFW_REPEAT) )
      {
        int code = info.code;
        // System.out.println("code: " + code + " mods: " + info.mods );

        if( code == GLFW_KEY_X && (info.mods & 1) == 0 ){// x for left
          if( controlEye ){
            eye.x[0] -= spaceAmount;
            updateView();
          }
          else{// control spotlight
            Light.get( 0 ).shiftBy( -spaceAmount, 0, 0 );
          }
        }
        else if( code == GLFW_KEY_X && (info.mods & 1) == 1 ){// X for right
          if( controlEye ){
            eye.x[0] += spaceAmount;
            updateView();
          }
          else{// control spotlight
            Light.get( 0 ).shiftBy( spaceAmount, 0, 0 );
          }
        }

        else if( code == GLFW_KEY_Y && (info.mods & 1) == 0 ){// y for near
          if( controlEye ){
            eye.x[1] -= spaceAmount;
            updateView();
          }
          else{// control spotlight
            Light.get( 0 ).shiftBy( 0, -spaceAmount, 0 );
          }
        }
        else if( code == GLFW_KEY_Y && (info.mods & 1) == 1 ){// Y for far
          if( controlEye ){
            eye.x[1] += spaceAmount;
            updateView();
          }
          else{// control spotlight
            Light.get( 0 ).shiftBy( 0, spaceAmount, 0 );
          }
        }

        else if( code == GLFW_KEY_Z && (info.mods & 1) == 0 ){// z for down
          if( controlEye ){
            eye.x[2] -= spaceAmount;
            updateView();
          }
          else{// control spotlight
            Light.get( 0 ).shiftBy( 0, 0, -spaceAmount );
          }
        }
        else if( code == GLFW_KEY_Z && (info.mods & 1) == 1 ){// Z for up
          if( controlEye ){
            eye.x[2] += spaceAmount;
            updateView();
          }
          else{// control spotlight
            Light.get( 0 ).shiftBy( 0, 0, spaceAmount );
          }
        }

        else if( code == GLFW_KEY_R && (info.mods & 1) == 0 ){// r  rotate
          if( controlEye ){
            azimuth -= angleAmount;
            if( azimuth < 0 )
              azimuth += 360;
            updateView();
          }
          else{// control spotlight
            Light.get( 0 ).turn( -angleAmount );
          }
        }
        else if( code == GLFW_KEY_R && (info.mods & 1) == 1 ){// R  rotate
          if( controlEye ){
            azimuth += angleAmount;
            if( azimuth >= 360 )
              azimuth -= 360;
            updateView();
          }
          else{// control spotlight
            Light.get( 0 ).turn( angleAmount );
           }
        }

        else if( code == GLFW_KEY_T && (info.mods & 1) == 0 ){// t tilt 
          if( controlEye ){
            altitude -= angleAmount;
            if( altitude >= 89 )
              altitude = 89;
            updateView();
          }
          else{// control spotlight
            Light.get( 0 ).tilt( -angleAmount );
          }
        }
        else if( code == GLFW_KEY_T && (info.mods & 1) == 1 ){// T tilt 
          if( controlEye ){
            altitude += angleAmount;
            if( altitude >= 89 )
              altitude = 89;
            updateView();
          }
          else{// control spotlight
            Light.get( 0 ).tilt( angleAmount );
          }
        }

        else if( code == GLFW_KEY_SPACE ){
          controlEye = !controlEye;
          System.out.println( controlEye ? "*** controlling eye" :
                                           "*** controlling spotlight" );
        }
   
      }// input event is a key

      else if ( info.kind == 'm' )
      {// mouse moved
      //  System.out.println( info );
      }

      else if( info.kind == 'b' )
      {// button action
       //  System.out.println( info );
      }

    }// loop to process all input events

  }

  protected void update()
  {
    for( int k=0; k<list2.size(); k++ ){
      list2.get( k ).rotate( 0.5, 0, 0, 1 );
    }
  }

  protected void display()
  {
    // System.out.println( "******************* " + getStepNumber() );

    GL11.glClear( GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT );

    // send over the uniform variables
    GL20.glUniformMatrix4fv( projMatrixLoc, false, proj.toBuffer() );
        Util.error( "after sending proj uniform");
    GL20.glUniformMatrix4fv( viewMatrixLoc, false, view.toBuffer() );
        Util.error( "after sending view uniform");
    GL20.glUniform4fv( eyeLoc, eye.toBuffer() );
        Util.error( "after sending eye uniform");

    Light.sendUniformInfo();

    // immobile soup is unchanging, just draw it each time
    first.draw();

    // rebuild mobile soup each time
    second.cleanup();
    second = new TriSoup( list2 );
    second.draw();

  }

}// Version1
