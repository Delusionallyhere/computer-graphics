#version 330 core

// attribute variables
layout (location = 0 ) in vec4 vertexPosition;
layout (location = 1 ) in vec4 normal;
layout (location = 2 ) in vec4 emi;
layout (location = 3 ) in vec4 ambi;
layout (location = 4 ) in vec4 diff;
layout (location = 5 ) in vec4 spec;
layout (location = 6 ) in float shini;

// uniform variables

// for viewing and perspective projection:
uniform mat4 proj;
uniform mat4 view;
uniform vec4 eye;

// for lights
uniform int numLights;  // number of lights in scene
uniform vec4 globalAmbi; // global ambient light

// a LightInfo variable can hold all the info for a single light
struct LightInfo{
  int lightNumber;  // lights are numbered 0, 1, 2, ...
  int on;           // C boolean
  vec4 position;    // position of light in scene or direction of light
                    // depending on whether positional
  vec4 spotlight;   // direction of spotlight
  float cutoff;     // cutoff angle for spotlight
  float exponent;   // spotlight exponent
  vec4 ambi, diff, spec;  // colors of the light
};

uniform LightInfo lights[8];  // info for all the lights

// out variables
out vec4 color;

// header so don't have to put before main
vec4 computeColor( int k );

void main(void)
{
  color = emi + globalAmbi * ambi;

  for( int k=0; k<numLights; k++ ){
    color = color + computeColor( k ); 
  }

  gl_Position = proj * view * vertexPosition;

}

vec4 computeColor( int k ){

  vec4 colorK;

  // compute the vectors needed:

  vec4 u = normalize( lights[k].position - vertexPosition );
  vec4 w = -u;

  float nDotU = dot( normal, u );

  vec4 r = (2*nDotU)*normal - u;  // is normalized if normal and u are

  vec4 e = normalize( eye - lights[k].position );

  // spotlight factor sigma
  float sigma;
    
  if( lights[k].cutoff > 90 )
     sigma = 1;   // cutoff meaningless so no spotlight effect
  else{// is a spotlight
     float actual = dot( w, lights[k].spotlight ); // cosine of angle between
     if( actual < cos( lights[k].cutoff ) ){// outside
        sigma = 0;
     }
     else{// in the cone
        sigma = pow( actual, lights[k].exponent );
     } 
  }// is a spotlight

   colorK = sigma*
           ( lights[k].ambi * ambi + 
             max( nDotU, 0 )* lights[k].diff * diff +
             pow( max( dot( r, e ), 0 ), shini ) * lights[k].spec * spec 
           );

  return colorK;

}// computeCOlor
