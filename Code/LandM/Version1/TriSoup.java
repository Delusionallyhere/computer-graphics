/*
   hold a collection of triangles,
   with OpenGL stuff needed to draw them

   draw  draws all the triangles, checking whether
         they need to be shipped to the GPU, and if so,
         it ships them first
*/

import java.util.ArrayList;
import java.nio.FloatBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

public class TriSoup{

  private ArrayList<Triangle> tris;  // holds the triangles
  private boolean loaded;  // remember whether the triangles have been shipped
                           // to GPU

  // buffers to hold data ready to be shipped to GPU
  private FloatBuffer posBuffer;  // holds the positions
  private FloatBuffer normalBuffer;  // holds the normals
  private FloatBuffer emiBuffer, ambiBuffer, diffBuffer, specBuffer,
                      shiniBuffer;

  private int vaoHandle;  // handle to loaded vertex array object

  private int posHandle;  // handle for positions VBO
  private int normalHandle;  // etc.
  private int emiHandle, ambiHandle, diffHandle, specHandle, shiniHandle; 

  public TriSoup(){
    tris = new ArrayList<Triangle>();
    loaded = false;
  }

  // create soup from list of Thing instances
  public TriSoup( ArrayList<Thing> list ){
    tris = new ArrayList<Triangle>();
    loaded = false;

    for( int k=0; k<list.size(); k++ ){
      list.get(k).add( this );
    }

  }

  // add given triangle to the list
  // (and note now need loading)
  public void add( Triangle tri ){
    tris.add( tri );
    noteChange();
  }
  
  // create string showing contents of the soup (no buffers)
  public String toString(){
    String s = "Have " + tris.size() + " triangles:\n";
    for( int k=0; k<tris.size(); k++ ){
      s += tris.get(k) + "\n";
    }
    return s;
  }

  // set everything up to send vertex data for
  // the triangles to GPU
  private void ship(){

//    System.out.println("ship from " + this );

    // copy tris data into buffers:
    fillBuffers();

    // set up vertex array object
    vaoHandle = GL30.glGenVertexArrays();
           Util.error("after generate single vertex array");
    GL30.glBindVertexArray( vaoHandle );
           Util.error("after bind the vao");
//           System.out.println("vao is " + vaoHandle );

    // now connect the buffers
    posHandle = connect( posBuffer, 0, 4 );
    normalHandle = connect( normalBuffer, 1, 4 );
    emiHandle = connect( emiBuffer, 2, 4);
    ambiHandle = connect( ambiBuffer, 3, 4 );
    diffHandle = connect( diffBuffer, 4, 4 );
    specHandle = connect( specBuffer, 5, 4 );
    shiniHandle = connect( shiniBuffer, 6, 1 );

  }

  private int connect( FloatBuffer buffer, 
                        int attribNum, int size ){

//System.out.println("TriSoup.connect to attrib # " + attribNum +
//" with size " + size + " this buffer: " );
//Util.showBuffer( "", buffer );

    int handle = GL15.glGenBuffers();

    GL15.glBindBuffer( GL15.GL_ARRAY_BUFFER, handle );
             Util.error("after bind handle");
    GL15.glBufferData( GL15.GL_ARRAY_BUFFER,
                               buffer, GL15.GL_STATIC_DRAW );
             Util.error("after set data");
  
    // enable the vertex array attribute
    GL20.glEnableVertexAttribArray( attribNum );
             Util.error("after enable attrib " + attribNum );

    GL15.glBindBuffer( GL15.GL_ARRAY_BUFFER, handle );
             Util.error("after bind buffer");
    GL20.glVertexAttribPointer( attribNum, size, GL11.GL_FLOAT, false, 0, 0 );
             Util.error("after do vertex attrib pointer");

    loaded = true;
    return handle;

  }// connect

  public void draw(){

    if( !loaded )
      ship();

    // activate vao
    GL30.glBindVertexArray( vaoHandle );
           Util.error("after bind vao");

    // draw the buffers
    GL11.glDrawArrays( GL11.GL_TRIANGLES, 0, 3 * tris.size() );
           Util.error("after draw arrays");

  }// draw

  // a change has happened, so if is first such,
  // clean up the obsolete buffer stuff, and
  // either way set  loaded
  private void noteChange(){
    if( loaded ){
      // cleanup();
    }
 
    loaded = false;
  }// noteChange

  // clean up buffer stuff due to change in
  // tris forcing another load
  public void cleanup(){

    GL30.glDeleteVertexArrays( vaoHandle );
    GL15.glDeleteBuffers( posHandle );
    GL15.glDeleteBuffers( normalHandle );
    GL15.glDeleteBuffers( emiHandle );
    GL15.glDeleteBuffers( ambiHandle );
    GL15.glDeleteBuffers( diffHandle );
    GL15.glDeleteBuffers( specHandle );
    GL15.glDeleteBuffers( shiniHandle );

  }// cleanup

  // scan tris and fill buffers
  private void fillBuffers(){
    // create buffers with correct space
    posBuffer = Util.createFloatBuffer( tris.size() * 12 );
    normalBuffer = Util.createFloatBuffer( tris.size() * 12 );
    emiBuffer = Util.createFloatBuffer( tris.size() * 12 );
    ambiBuffer = Util.createFloatBuffer( tris.size() * 12 );
    diffBuffer = Util.createFloatBuffer( tris.size() * 12 );
    specBuffer = Util.createFloatBuffer( tris.size() * 12 );
    shiniBuffer = Util.createFloatBuffer( tris.size() * 3 );

    // scan tris and fill buffers:
    for( int k=0; k<tris.size(); k++ ){
      tris.get(k).toBuffers( posBuffer, normalBuffer,
                             emiBuffer, ambiBuffer, diffBuffer, specBuffer,
                             shiniBuffer );
    }

    posBuffer.rewind();
    normalBuffer.rewind();
    emiBuffer.rewind();
    ambiBuffer.rewind();
    diffBuffer.rewind();
    specBuffer.rewind();
    shiniBuffer.rewind();

  }// fillBuffers

}
