/*
   this class provides individual light
   instances and global features of
   lighting model
*/

import java.nio.FloatBuffer;

import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;


public class Light{

  private static Vec4 globalAmbi;  // global ambient light 
                                   // independent of any individual light
  private static int globalAmbiLoc, numLightsLoc;
  
  // can change global ambient light for the model as desired
  public static void setGlobalAmbient( double red, double green, double blue,
                                       double alpha ){
    globalAmbi = new Vec4( red, green, blue, alpha );
  }

  // maintain list of all lights
  private static int currentLightNumber = 0;
  private static Light[] lights;

  // initialize the lighting model, prepared to create the 
  // individual lights
  public static void init( int programHandle, int numLights, 
                     double red, double green, double blue, double alpha ){

    setGlobalAmbient( red, green, blue, alpha );
    globalAmbiLoc = GL20.glGetUniformLocation( programHandle, "globalAmbi" );
System.out.println("got " + globalAmbiLoc + " loc for globalAmbi uniform");
     
    lights = new Light[ numLights ];

    numLightsLoc = GL20.glGetUniformLocation( programHandle, "numLights" );
System.out.println("got " + numLightsLoc + " loc for num lights uniform");

  }

  // do all the OpenGL stuff to send all the info about
  // the light model and the individual lights to the GPU
  // For connecting to the shaders, individual light k
  // is sent to two uniform 4 by 4 matrices
  // named  light<k>Geo and light<k>Color
  // (using matrices as a convenient way to send a bunch of data)
  public static void sendUniformInfo(){

    // send number of lights
    GL20.glUniform1i( numLightsLoc, lights.length );
        Util.error( "after sending number of lights");

    // send global ambient light
    GL20.glUniform4fv( globalAmbiLoc, globalAmbi.toBuffer() );
        Util.error( "after sending globalAmbi uniform");
    
    // send all the individual lights
    for( int k=0; k<lights.length; k++ ){
      lights[ k ].sendUniforms();
    }
  
  }

  public static Light get( int index ){
    if( 0<=index && index<lights.length )
      return lights[index];
    else{
      System.out.println("illegal light index " + index );
      System.exit(1);
      return null;
    }
  }

  // name all the components of a light that are part of the
  // shader LightInfo struct
  private static String[] componentNames =
   { "lightNumber", "on", "position", "spotlight",
     "cutoff", "exponent", "ambi", "diff", "spec",
     "atten0", "atten1", "atten2" };

  //--------------------------------------------------

  private int lightNumber;  // lights are numbered 0, 1,...
  private boolean on;   // is this individual light currently on

  private Vec4 position;  // position of the light
  private Vec4 spotlight;  // direction of spotlight
  private double azimuth, altitude;  // derive spotlight from these

  private double cutoff, exponent;  // spotlight cutoff angle and exponent

  private Vec4 ambi, diff, spec;  // colors of this light

  // buffers to hold the non-scalar components for shipping over
  private FloatBuffer posBuffer, spotBuffer, ambiBuffer,
                      diffBuffer, specBuffer;

  // locations in the shader for all the components of a light
  private int[] locs;

  public Light( int programHandle, Vec4 p, 
                 double azi, double alt,
                 double cutoffAngle, double spotlightExponent,
                 Vec4 amb, Vec4 dif, Vec4 spe ) {
    
    on = true;

    position = p;

    azimuth = azi;  altitude = alt;
    updateSpotlight();

    cutoff = cutoffAngle;  
    exponent = spotlightExponent;

    ambi = amb;  diff = dif;  spec = spe;

    lightNumber = currentLightNumber;    currentLightNumber++;
    lights[ lightNumber ] = this;

    // set up handles for uniforms this light needs
    locs = new int[ componentNames.length ];
System.out.println("Checking locs for light " + lightNumber );
    for( int k=0; k<locs.length; k++ ){
System.out.print("loc for " + 
                 "lights[" + lightNumber + "]." + componentNames[k]  + " is " );

      locs[k] = GL20.glGetUniformLocation( programHandle,
                 "lights[" + lightNumber + "]." + componentNames[k] );
                Util.error( "after getting loc of this uniform" );
System.out.println( locs[k] );
    }
    
  }

  // send data for this light packed into two mat4's
  // (don't actually need the Mat4 objects, just the buffers)
  // (currently always recreate the info and send every frame)
  private void sendUniforms(){

    GL20.glUniform1i( locs[0], lightNumber );
    GL20.glUniform1i( locs[1], on ? 1 : 0 );

    posBuffer = position.toBuffer();
    GL20.glUniform4fv( locs[2], posBuffer );
    
    spotBuffer = spotlight.toBuffer();
    GL20.glUniform4fv( locs[3], spotBuffer );

    GL20.glUniform1f( locs[4], (float) cutoff );
    GL20.glUniform1f( locs[5], (float) exponent );
    
    ambiBuffer = ambi.toBuffer();
    GL20.glUniform4fv( locs[6], ambiBuffer );
    diffBuffer = diff.toBuffer();
    GL20.glUniform4fv( locs[7], diffBuffer );
    specBuffer = spec.toBuffer();
    GL20.glUniform4fv( locs[8], specBuffer );

  }// sendUniforms

  // compute spotlight direction from given angles
  private void updateSpotlight(){
    double c = Math.cos( Math.toRadians( azimuth ) );
    double s = Math.sin( Math.toRadians( azimuth ) );
    double c2 = Math.cos( Math.toRadians( altitude ) );
    double s2 = Math.sin( Math.toRadians( altitude ) );

    spotlight = new Vec4( c*c2, s*c2, s2, 0 );

    System.out.println("Mobile light at " + position +
      " azimuth= " + azimuth + " altitude= " + altitude );
    System.out.println(" spotlight dir is " + spotlight );
  }

  //----------------------------------------------------
  // some lights need to be interactively changed after
  // construction, which these methods allow

  // rotate about the z axis by given amount
  public void turn( double dazi ){

    azimuth += dazi;

    if( azimuth < 0 )
      azimuth += 360;
    else if( azimuth > 360 )
      azimuth -= 360;

    updateSpotlight();
  }

  // change the altitude by given amount
  public void tilt( double dalt ){

    altitude += dalt;
    
    if( altitude < -89 )
      altitude = -89;
    else if( altitude > 89 )
      altitude = 89;

    updateSpotlight();
  }

  // add vector (dx,dy,dz) to the light position
  public void shiftBy( double dx, double dy, double dz ){
    position = position.plus( new Vec4(dx, dy, dz,0) );
    updateSpotlight();
  }

  public void turnOff(){
    on = false;
  }

  public void turnOn(){
    on = true;
  }

}
