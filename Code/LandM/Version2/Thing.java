import java.util.Scanner;
/* 
   Things with material properties (uniform over
   all vertices of the same conceptual face)
   and normal vectors, with either flat
   or smooth shading
*/

public class Thing{

  private String kind;
  private boolean flat;

  private Vec4[] modelVerts;  
  private Vec4[][] modelNormals;
  private int[][] tris;

  private Vec4[] triNormals;  // normals to each triangle
                    
  private int[] materials;  // material index for each triangle

  private Vec4[] verts;       // current transformed vertices
  private Vec4[][] normals;   // current transformed normal vectors
  private Mat4 rotate, translate; 

  public Thing( Scanner input ){
   
    kind = input.next();     // like box, cone, cyl
    String surface = input.next();  // flat or smooth
    flat = surface.equals( "flat" );

    // rotation
    double angle = input.nextDouble();
    double axisX = input.nextDouble();
    double axisY = input.nextDouble();
    double axisZ = input.nextDouble();
    
    // translate
    double transX = input.nextDouble();
    double transY = input.nextDouble();
    double transZ = input.nextDouble();
    
    rotate = Mat4.rotate( angle, axisX, axisY, axisZ );
    translate = Mat4.translate( transX, transY, transZ );

    // build the instance variables
    // depending on the kind of thing

    if( kind.equals( "box" ) ){
      double width = input.nextDouble();
      double length = input.nextDouble();
      double height = input.nextDouble();

      modelVerts = new Vec4[8];
      modelVerts[0] = new Vec4( -width/2, -length/2, -height/2 );
      modelVerts[1] = new Vec4( +width/2, -length/2, -height/2 );
      modelVerts[2] = new Vec4( -width/2, +length/2, -height/2 );
      modelVerts[3] = new Vec4( +width/2, +length/2, -height/2 );
      modelVerts[4] = new Vec4( -width/2, -length/2, +height/2 );
      modelVerts[5] = new Vec4( +width/2, -length/2, +height/2 );
      modelVerts[6] = new Vec4( -width/2, +length/2, +height/2 );
      modelVerts[7] = new Vec4( +width/2, +length/2, +height/2 );

      tris = new int[][] 
              { {0,1,5}, {0,5,4}, {1,3,7}, {1,7,5}, {2,0,4}, {2,4,6},
                {3,2,6}, {3,6,7}, {4,5,7}, {4,7,6}, {2,3,1}, {2,1,0}   };

      materials = new int[] {0,1,2,3,4,5,6,7,8,9,10,11};

      computeFlatNormals();

      if( !flat ){// customize smooth normals
        // point outward from origin to each vertex 
        // (smooth shading not a very natural thing to do,
        //  so doesn't matter a lot)
        for( int k=0; k<tris.length; k++ ){
          for( int j=0; j<3; j++ ){
             modelNormals[k][j] = modelVerts[ tris[k][j] ].normalize();
          }
        }

      }

    }// box

    else if( kind.equals( "wall" ) ){
      double width = input.nextDouble();
      double height = input.nextDouble();
      int numX = input.nextInt();
      int numZ = input.nextInt();

      modelVerts = new Vec4[ (numX+1)*(numZ+1) ];

      double x = 0, z = 0;
      double dx = width/numX, dz = height/numZ;
      int index = 0;

      for( int j=0; j<=numZ; j++ ){
        for( int k=0; k<=numX; k++ ){
          modelVerts[index] = new Vec4( x, 0, z );
          index++;
          x += dx;
        }
        x = 0;
        z += dz;
      }

System.out.println("wall vertices: ");
for( int k=0; k<modelVerts.length; k++ )
  System.out.println( k + ": " + modelVerts[k] );
      
      tris = new int[ numX*numZ*2 ][3];

      index = 0;
      int count = 0;

      while( count < numX*numZ*2 ){
        for( int k=0; k<numX; k++ ){// go across a row

          tris[count][0] = index; 
          tris[count][1] = index+1; 
          tris[count][2] = index+numX+2;
          count++;

          tris[count][0] = index; 
          tris[count][1] = index+numX+2; 
          tris[count][2] = index+numX+1;
          count++;

          index ++;
        }
        index ++;  // skip last column in each row
      }

System.out.println("wall triangles: ");
for( int k=0; k<tris.length; k++ )
  System.out.println( tris[k][0] + " " + tris[k][1] + " " + tris[k][2] );

      int mat = getMaterial( input );  // read one material for all tris
      materials = new int[ tris.length ];
      for( int k=0; k<tris.length; k++ ){
        materials[k] = mat;
      }

      computeFlatNormals();

      // don't allow non-flat normals

    }// wall

    else if( kind.equals( "tri" ) ){
      modelVerts = new Vec4[3];
      modelVerts[0] = new Vec4( input.nextDouble(), input.nextDouble(), 
                                input.nextDouble() );
      modelVerts[1] = new Vec4( input.nextDouble(), input.nextDouble(), 
                                input.nextDouble() );
      modelVerts[2] = new Vec4( input.nextDouble(), input.nextDouble(), 
                                input.nextDouble() );

      tris = new int[][] 
              { {0, 1, 2 } };

      materials = new int[1];
      materials[0] = getMaterial( input );

      computeFlatNormals();
 
      // leave modelNormals as the triNormals

    }// tri

    else if( kind.equals( "cone" ) ){
      
      double radius = input.nextDouble();
      double height = input.nextDouble();
      int num = input.nextInt();

      modelVerts = new Vec4[num+2];
      // make top point
      modelVerts[0] = new Vec4( 0, 0, height );

      // make base border vertices
      angle = 0;
      double change = 360.0/num;
      for( int k=1; k<=num; k++ ){
        double ang = Math.toRadians( angle );
        double c = Math.cos( ang ), s = Math.sin( ang );
        modelVerts[k] = new Vec4( radius*c, radius*s, 0 );
        angle += change;        
      }

      // make center of the base polygon point
      modelVerts[num+1] = new Vec4( 0, 0, 0 );

      tris = new int[2*num][3];
      int index = 0;  // index into collection of triangles

      // create side triangles
      for( int k=1; k<=num; k++ ){
        // put in side triangles k->k+1->0

        tris[index][0] = k;  
        if( k!=num ) tris[index][1] = k+1; else tris[index][1] = 1;
        tris[index][2] = 0;  

        index++;
      }      

      for( int k=1; k<=num; k++ ){
        // put in base triangles k->num+1->k+1

        tris[index][0] = k;
        tris[index][1] = num+1;
        if( k!=num ) tris[index][2] = k+1; else tris[index][2] = 1;

        index++;
      }

      int materialOne = getMaterial( input );
      int materialTwo = getMaterial( input );

      materials = new int[tris.length];
      for( int k=0; k<tris.length; k++ ){
        materials[k] = k%2==0 ? materialOne : materialTwo;
      }

      computeFlatNormals();

      if( !flat ){// customize smooth normals

        Vec4 up = new Vec4( 0, 0, 1, 0 );

        for( int k=0; k<num-1; k++ ){// tris 0 through num-2
          modelNormals[k][0] = coneNormal( k+1, radius, height );
          modelNormals[k][1] = coneNormal( k+2, radius, height );
          modelNormals[k][2] = up;
        }
        // tri num-1 on side:
          modelNormals[num-1][0] = coneNormal( num, radius, height );
          modelNormals[num-1][1] = coneNormal( 1, radius, height );
          modelNormals[num-1][2] = up;
        
        // all base triangles (num through 2*num-1)
        // and verts have normal straight down
        Vec4 down = new Vec4( 0, 0, -1, 0 );
        for( int k=num; k<2*num; k++ ){
          for( int j=0; j<3; j++ ){
            modelNormals[k][j] = down;
          }
        }

      }// customize smooth normals

    }// cone

    else if( kind.equals( "tube" ) ){
      
      double radius = input.nextDouble();
      double height = input.nextDouble();
      int num = input.nextInt();

      modelVerts = new Vec4[2*num];

      // make base and top border vertices
      angle = 0;
      double change = 360.0/num;
      for( int k=0; k<=num-1; k++ ){
        double ang = Math.toRadians( angle );
        double c = Math.cos( ang ), s = Math.sin( ang );
        modelVerts[k] = new Vec4( radius*c, radius*s, -height/2 );
        modelVerts[k+num] = new Vec4( radius*c, radius*s, height/2 );
        angle += change;        
      }

      tris = new int[2*num][3];

      int index = 0;  // index into collection of triangles

      // create side rectangles (two triangles each)
      for( int k=0; k<num; k++ ){
        // put in side triangles k->k+1+num->k+num, k->k+1->k+1+num

        tris[index][0] = k;  
        if( k!=num-1 ) tris[index][1] = k+1+num; else tris[index][1] = num;
        tris[index][2] = k+num;  

        index++;

        tris[index][0] = k;  
        if( k!=num-1 ) tris[index][1] = k+1; else tris[index][1] = 0;
        if( k!=num-1 ) tris[index][2] = k+1+num; else tris[index][2] = num;

        index++;

      }      

      int materialOne = getMaterial( input );
      int materialTwo = getMaterial( input );

      materials = new int[tris.length];
      for( int k=0; k<tris.length; k++ ){
        materials[k] = k%2==0 ? materialOne : materialTwo;
      }

      computeFlatNormals();

      if( !flat ){// customize smooth normals

        index = 0;  // triangle number

        for( int k=0; k<num; k++ ){// go through all base vertices
          Vec4 v1 = modelVerts[k];
          Vec4 v2 = modelVerts[ (k+1) % num ];

          Vec4 norm1 = new Vec4( v1.x[0], v1.x[1], 0, 0 ).normalize();
          Vec4 norm2 = new Vec4( v2.x[0], v2.x[1], 0, 0 ).normalize();

          modelNormals[index][0] = norm1;
          modelNormals[index][1] = norm1;
          modelNormals[index][2] = norm2;

          index++;

          modelNormals[index][0] = norm1;
          modelNormals[index][1] = norm2;
          modelNormals[index][2] = norm2;

          index++;
        }

      }// customize smooth normals

    }// tube

    else{
      System.out.println("Unknown kind of thing");
      System.exit(1);
    }

    // set up workspace for verts, normals
    verts = new Vec4[ modelVerts.length ];
    normals = new Vec4[ tris.length ][3];

    transform();

  }// end of constructor

  // read info for material, try to interpret as index,
  // if fails, try to interpret as a material name,
  // and return index either way
  private int getMaterial( Scanner input ){
    int index;
    String word = input.next();
    try{
      index = Integer.parseInt( word );
    }
    catch(NumberFormatException e){
      index = Material.get( word );
    }
 
    return index;

  }// getMaterial

  // compute normal to surface of cone at base vertex
  // modelVerts[j]
  private Vec4 coneNormal( int j, double r, double h ){
    Vec4 v = modelVerts[j];
    return new Vec4( v.x[0]/r, v.x[1]/r, -r/h, 0 ).normalize();
  }

  // compute triangle normals (triNormals)
  // and then the modelNormals if flat
  // (called in each kind of thing, after modelVerts and
  //  tris are built, followed by
  //  customized smooth normals computations)
  private void computeFlatNormals(){

    // compute cross product for actual normal to each triangle
    triNormals = new Vec4[ tris.length ];

    for( int k=0; k<tris.length; k++ ){
      // for convenience, form a,b,c
      Vec4 a = modelVerts[ tris[k][0] ];
      Vec4 b = modelVerts[ tris[k][1] ];
      Vec4 c = modelVerts[ tris[k][2] ];

      Vec4 bMinusA = b.minus(a);
      Vec4 cMinusA = c.minus(a);

      // note: here is where the ccw order of model vertices
      //       matters, so we get outward pointing normals!
      triNormals[ k ] = bMinusA.cross( cMinusA );

      // normalize the normal
      triNormals[ k ] = triNormals[ k ].normalize();
    }

    // set up modelNormals as triNormals in case not changing

    modelNormals = new Vec4[ tris.length ][3];

    for( int k=0; k<tris.length; k++ ){
      for( int j=0; j<3; j++ )
        modelNormals[k][j] = triNormals[k];
    }

  }// computeFlatNormals

  // use current rotate and translate to compute verts 
  // from modelVerts, and transform normals correctly
  private void transform(){
   
    // transform modelVerts
    for( int k=0; k<verts.length; k++ ){
      verts[k] =  translate.mult( rotate.mult( modelVerts[k] ) );
    }

    // transform modelNormals
    for( int k=0; k<tris.length; k++ ){
      for( int j=0; j<3; j++ ){
        normals[k][j] = rotate.mult( modelNormals[k][j] );
      }
    }
  }

  // add all the triangles for this thing to
  // the given triangle soup
  public void add( TriSoup soup ){
    for( int k=0; k<tris.length; k++ ){
      Vertex v1 = new Vertex( verts[tris[k][0]], normals[k][0], materials[k] );
      Vertex v2 = new Vertex( verts[tris[k][1]], normals[k][1], materials[k] );
      Vertex v3 = new Vertex( verts[tris[k][2]], normals[k][2], materials[k] );
      soup.add( new Triangle( v1, v2, v3 ) );
    }
  }    

  public String toString(){
    return kind + " center: " + translate.a[0][3] + " " +
               translate.a[1][3] + " " + translate.a[2][3];
  }

  // multiply current rotation matrix on left by one that 
  // rotates as specified
  public void rotate( double alpha, double x, double y, double z ){
    rotate = Mat4.rotate( alpha, x, y, z ).mult( rotate );
    transform();
  }
}
