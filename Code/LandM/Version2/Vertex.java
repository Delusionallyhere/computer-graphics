/*
  now a "vertex" is a package of
  all the attributes for a vertex
  in OpenGL, namely position in space,
  normal vector, and material index
  (when ship to buffers we'll look up
   the actual bunch of numbers in Material)

  Never read a vertex directly from data file,
  is always constructed by someone else from
  data (for example, the normal vector is
  always built, never read from data)
*/

import java.nio.FloatBuffer;

public class Vertex{

  private Vec4 position;
  private Vec4 normal;
  private int materialIndex;

  public Vertex( Vec4 pos, Vec4 n, int mi ){
    position = pos;
    normal = n;
    materialIndex = mi;
  }

  public String toString(){
    return "pos: " + position + " normal: " + normal +
            " matIndex: " + materialIndex + "\n";
  }

  // append data for this vertex to buffers
  public void toBuffers( FloatBuffer posBuffer, FloatBuffer normalBuffer,
    FloatBuffer emiBuffer, FloatBuffer ambiBuffer, FloatBuffer diffBuffer,
    FloatBuffer specBuffer, FloatBuffer shiniBuffer ){
    
    position.put( posBuffer );
    normal.put( normalBuffer );

    Material.mat.get( materialIndex ).put( emiBuffer, 
       ambiBuffer, diffBuffer, specBuffer, shiniBuffer );

  }// toBuffers

}
