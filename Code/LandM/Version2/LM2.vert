/*
   This vertex shader receives attribute variables
   for the vertex and sends them out, along with
   the usual gl_Position, interpolated for each 
   fragment
*/

#version 330 core

// attribute variables
layout (location = 0 ) in vec4 vertexPosition;
layout (location = 1 ) in vec4 normal;
layout (location = 2 ) in vec4 emi;
layout (location = 3 ) in vec4 ambi;
layout (location = 4 ) in vec4 diff;
layout (location = 5 ) in vec4 spec;
layout (location = 6 ) in float shini;

// for viewing and perspective projection:
uniform mat4 proj;
uniform mat4 view;

// out variables
out vec4 vertexPositionFrag;
out vec4 normalFrag;
out vec4 emiFrag;
out vec4 ambiFrag;
out vec4 diffFrag;
out vec4 specFrag;
out float shiniFrag;

void main(void)
{
  // send out the interpolated attribute variables
  vertexPositionFrag = vertexPosition;
  normalFrag = normal;
  emiFrag = emi;
  ambiFrag = ambi;
  diffFrag = diff;
  specFrag = spec;
  shiniFrag = shini;

  // oops---need to transform normalFrag similarly
  // ???

  gl_Position = proj * view * vertexPosition;

}
