/*
   this class encapsulates all material
   properties for convenience
*/

import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.nio.FloatBuffer;

public class Material{

  public String kind;  // for searching and clarity in data files

  public Vec4 emissive;
  public Vec4 ambient;
  public Vec4 diffuse;
  public Vec4 specular;
  public double shininess;

  // read a line of doubles to produce a material
  public Material( Scanner input ){
    kind = input.next(); 
    emissive = new Vec4( input );
    ambient = new Vec4( input );
    diffuse = new Vec4( input );
    specular = new Vec4( input );
    shininess = input.nextDouble();

    input.nextLine();
  }

  public String toString(){
    return "{" + kind + emissive.clampedToString() + 
             ambient.clampedToString() + diffuse.clampedToString() +
             specular.clampedToString() + shininess + "}";
  }

  // send all the data for this material to the given buffers
  public void put( FloatBuffer emiBuffer, FloatBuffer ambiBuffer, 
                   FloatBuffer diffBuffer, FloatBuffer specBuffer, 
                   FloatBuffer shiniBuffer ){
    emissive.put( emiBuffer );
    ambient.put( ambiBuffer );
    diffuse.put( diffBuffer );
    specular.put( specBuffer );
    shiniBuffer.put( (float) shininess );
  }

  //-------------------------------- static stuff --------------------

  // handy list of materials
  public static ArrayList<Material> mat = new ArrayList<Material>();

  public static void load( Scanner input ){

    while( input.hasNext() ){
      // read another line of materials
      mat.add( new Material( input ) );
    }

  }

  // get index in materials list mat of material with
  // given name
  public static int get( String name ){
    for( int k=0; k<mat.size(); k++ ){
      if( mat.get(k).kind.equals( name ) ){
        return k;
      }
    }
    System.out.println("unknown material [" + name + "]");
    return -1;  // probably will crash the application
  }

  // show all materials nicely
  public static void showAll(){
    for( int k=0; k<mat.size(); k++ ){
      System.out.println( mat.get(k) );
    }
  }

  public static void main(String[] args) throws Exception{ 
    Scanner input = new Scanner( new File( "standard.mat" ) );
    Material.load( input );
    Material.showAll();
  }

}
