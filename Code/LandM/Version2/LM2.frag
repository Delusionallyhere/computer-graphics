#version 330 core

// receive interpolated values per fragment
in vec4 vertexPositionFrag;
in vec4 normalFrag;
in vec4 emiFrag;
in vec4 ambiFrag;
in vec4 diffFrag;
in vec4 specFrag;
in float shiniFrag;

// uniform variables

uniform vec4 eye;

// data for lights
uniform int numLights;  // number of lights in scene
uniform vec4 globalAmbi; // global ambient light

// a LightInfo variable can hold all the info for a single light
struct LightInfo{
  int lightNumber;  // lights are numbered 0, 1, 2, ...
  int on;           // C boolean
  int positional;   // C boolean  
  vec4 position;    // position of light in scene or direction of light
                    // depending on whether positional
  vec4 spotlight;   // direction of spotlight
  float cutoff;     // cutoff angle for spotlight
  float exponent;   // spotlight exponent
  vec4 ambi, diff, spec;  // colors of the light
  float atten0, atten1, atten2;  // attenuation factors
};

uniform LightInfo lights[8];  // info for all the lights

layout (location = 0 ) out vec4 fragColor;

// header so don't have to put before main
vec4 computeColor( int k );

void main(void)
{
  fragColor = emiFrag + globalAmbi * ambiFrag;

  for( int k=0; k<numLights; k++ ){
    fragColor = fragColor + computeColor( k ); 
  }

}

vec4 computeColor( int k ){

  vec4 colorK;

  if( lights[k].positional == 1 ){

    // compute the vectors needed:

    vec4 pMinusV = lights[k].position - vertexPositionFrag;
    float delta = length( pMinusV );  // distance between light and point
    vec4 u = normalize( lights[k].position - vertexPositionFrag );
    vec4 w = -u;

    // should normalize?
//    normalFrag = normalize( normalFrag );
    float nDotU = dot( normalFrag, u );

    vec4 r = (2*nDotU)*normalFrag - u;  // is normalized if normal and u are

    // Had this wrong?    
    // Now it's really wrong!
        vec4 e = normalize( eye - lights[k].position );
//    vec4 e = normalize( eye - vertexPositionFrag );

    // attenuation factor alpha
    float alpha = 1/
       (lights[k].atten0+lights[k].atten1*delta+lights[k].atten2*delta*delta);

    // spotlight factor sigma
    float sigma;
    
    if( lights[k].cutoff > 90 )
      sigma = 1;   // cutoff meaningless so no spotlight effect
    else{// is a spotlight
      float actual = dot( w, lights[k].spotlight ); // cosine of angle between
      if( actual < cos( lights[k].cutoff ) ){// outside
        sigma = 0;
      }
      else{// in the cone
        sigma = pow( actual, lights[k].exponent );
      } 
    }// is a spotlight

    colorK = alpha*sigma*
           ( lights[k].ambi * ambiFrag + 
             max( nDotU, 0 )* lights[k].diff * diffFrag +
        pow( max( dot( r, e ), 0 ), shiniFrag ) * lights[k].spec * specFrag 
           );

  }// positional light
  else{// directional light

    // directional light not implemented
    colorK = vec4(0,0,0,0);

  }

  return colorK;

}// computeCOlor
