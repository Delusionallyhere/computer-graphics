/*
   This vertex shader just receives position data
   and generates fragments with just position data
   for ray tracing
*/

#version 330 core

// attribute variables
layout (location = 0 ) in vec4 vertexPosition;

// out variables
out float x, y;

void main(void)
{
  // send out the interpolated attribute variables
  x = vertexPosition.x;
  y = vertexPosition.y;

  // have to do this
  gl_Position = vertexPosition;

}
