/*
   this class provides individual light
   instances and global features of
   lighting model

  Modified from LM version to send
  uniforms:  lightPosition, ambi, diff, spec,
      and nothing else
*/

import java.nio.FloatBuffer;

import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;


public class Light{

  // name all the components of a light that are part of the
  // shader LightInfo struct
  private static String[] componentNames =
   { "position", "ambi", "diff", "spec" };

  //--------------------------------------------------

  private Vec4 position;  // position of the light
  private Vec4 ambi, diff, spec;  // colors of this light

  // buffers to hold the non-scalar components for shipping over
  private FloatBuffer posBuffer, ambiBuffer,
                      diffBuffer, specBuffer;

  // locations in the shader for all the components of a light
  private int[] locs;

  public Light( int programHandle, Vec4 p, 
                 Vec4 amb, Vec4 dif, Vec4 spe ){
    
    position = p;
    ambi = amb;  diff = dif;  spec = spe;

System.out.println("program handle: " + programHandle );

    // set up handles for uniforms this light needs
    locs = new int[ componentNames.length ];
    for( int k=0; k<locs.length; k++ ){
System.out.println("getting location for " + "light." + componentNames[k] );
      locs[k] = GL20.glGetUniformLocation( programHandle,
                 "light." + componentNames[k] );
                Util.error( "after getting loc " + k + " of this uniform" );
System.out.println( "light loc " + k + " is " + locs[k] );
    }
    
  }

  // send data for this light to uniforms
  public void sendUniforms(){

    posBuffer = position.toBuffer();
    GL20.glUniform4fv( locs[0], posBuffer );
    
    ambiBuffer = ambi.toBuffer();
    GL20.glUniform4fv( locs[1], ambiBuffer );

    diffBuffer = diff.toBuffer();
    GL20.glUniform4fv( locs[2], diffBuffer );

    specBuffer = spec.toBuffer();
    GL20.glUniform4fv( locs[3], specBuffer );

  }// sendUniforms

  public void move( double dx, double dy, double dz ){
    position = position.plus( new Vec4(dx, dy, dz, 0) );
  }

  public Vec4 getPosition(){
    return position;
  }

}
