#version 330 core

// receive interpolated values per fragment
in float x;   // position of fragment in space
in float y;

// uniform variables for the pixel grid in space
uniform vec3 eye, center, right, up;

// data for light (simplified light model)
struct LightInfo {
  vec4 position;
  vec4 ambi;
  vec4 diff;
  vec4 spec;
};

uniform LightInfo light;

// data for a bunch of spheres

uniform int numSpheres;

struct SphereInfo {
  vec4 center;    // geometry info for the sphere
  float radius;
  vec4 emi;       // material info for the sphere
  vec4 ambi;
  vec4 diff;
  vec4 spec;
  float shini;
};

uniform SphereInfo spheres[100];  // info for all the spheres in the scene

layout (location = 0 ) out vec4 fragColor;

float hitTime( int k, vec3 e, vec3 d );
vec4 view( vec3 p );

void main(void) {

/*
if ( eye == vec3(50,-250,50) ) {
  fragColor = vec4(1,0,0,1);
}
else {
  fragColor = vec4(0,1,0,1);
}
return;
*/
 
  int bestIndex = -1;
  float bestLambda = 0;

  vec3 d = normalize( center + x*right + y*up - eye );

  for( int k=0; k<numSpheres; k++ ) {

    float lambda = hitTime( k, eye, d );

    if( lambda >= 1 && (bestIndex==-1 || lambda < bestLambda ) ){
      bestLambda = lambda;
      bestIndex = k;
    }
  }

/*
    // stub:  turn bestIndex into color where -1 becomes black
    //        so if any hit was detected, should be red
    if ( bestLambda == 5 ) { // bestIndex == -1 ) {
       fragColor = vec4(1,0,0,1);
    }
    else {
       fragColor = vec4(0,1,0,1);
    }
    return;
*/

  if( bestIndex >= 0 ){

    // compute fragColor using lighting model:

    vec3 v = eye + bestLambda * d;  // where ray hits the first sphere
    vec3 n = normalize( v - spheres[bestIndex].center.xyz );
    vec3 u = normalize( light.position.xyz - v );
    vec3 e = normalize( eye - v );
    vec3 r = 2*dot(n,u)*n - u;

    fragColor = spheres[bestIndex].emi +
                light.ambi * spheres[bestIndex].ambi +
                max( 0, dot( u, n )) * light.diff * spheres[bestIndex].diff +
                pow(max( 0, dot( r, e )), spheres[bestIndex].shini)*
                      light.spec * spheres[bestIndex].spec;
  }
  else{// line from eye to fragment misses all spheres
    fragColor = vec4(1.0,1.0,1.0,1);
  }

}// main

// find time (lambda) for which 
//   p + lambda d
// hits sphere[k], or -1 if misses
float hitTime( int k, vec3 e, vec3 d ){

  vec3 c = vec3( spheres[k].center );

  vec3 cMinusE = c-e;

  float norm2CE = dot( cMinusE, cMinusE );
  float r = spheres[k].radius;

  if ( norm2CE < r*r ) {
    // e inside the sphere, so call it a miss
    return -1;
  }
  else {// not inside
     float dce = dot( d, cMinusE );
     if ( dce < 0 ) {
        return -1;  // ray going away from sphere
     }
     else {
        // d2 is really D^2 according to page 82
        float d2 = norm2CE - dce*dce;
        if ( d2 <= r*r ) {
           float mu = sqrt( r*r - d2 );
           return dce - mu;
        }
        else {
          return -1;
        }
     }
  }

}// hitTime

// uniformly convert arbitrary p in 3D
// to a vec4 color
vec4 view( vec3 p ) {
/*
   vec3 q = normalize(p);
   q = abs( q );
   return vec4( q, 1 );
*/

   if ( abs(p.x+p.y+p.z) > 0 ) {
      return vec4(1,0,0,1);
   }
   else {
      return vec4(0,1,0,1);
   }

}
