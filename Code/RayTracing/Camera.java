/*
   This class was borrowed from NaturalViewing,
   leave it intact but add facility to send
   e, a, r, u  as uniforms to GPU
*/

import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import org.lwjgl.system.MemoryUtil;

public class Camera {

   // uniform locations
   private int eyeLoc, centerLoc, rightLoc, upLoc;
   private FloatBuffer eyeBuffer, centerBuffer, rightBuffer, upBuffer;

   private Triple e, a;
   private Triple eMinusA;
   private Triple r;  // B-A
   private Triple u;  // C-A

   private double azi, alt;
   private double d;  // distance from E to A

   private double ea2, ba2, ca2;

   // given eye point, azimuth and altitude (in degrees)
   // and distance from eye to center of plexiglass
   // construct a camera
   public Camera( Triple eye, double azimuth, 
                  double altitude, double dist ) {
      e = eye;
      azi = azimuth;  alt = altitude;
      d = dist;
      
      eyeBuffer = MemoryUtil.memAllocFloat( 3 );
      centerBuffer = MemoryUtil.memAllocFloat( 3 );
      rightBuffer = MemoryUtil.memAllocFloat( 3 );
      upBuffer = MemoryUtil.memAllocFloat( 3 );

      update();

   }// constructor

   // recompute instance fields when one of them
   // changes (azi, alt, d, e)
   private void update() {
   
      // see page 33
      double alpha = Math.toRadians(azi);
      double beta = Math.toRadians(alt);
      double ca = Math.cos(alpha);
      double sa = Math.sin(alpha);
      double cb = Math.cos(beta);
      double sb = Math.sin(beta);
    
      a = new Triple( e.x + cb*d*ca,
                      e.y + cb*d*sa,
                      e.z + d*sb );

      eMinusA = e.subtract(a);
      r = Triple.zAxis.cross( eMinusA ).normalize();
      u = eMinusA.cross(r).normalize();
      // B = A +r, C = A + u

      ea2 = eMinusA.dot(eMinusA);
      ba2 = r.dot(r);  // bMinusA.dot(bMinusA);
      ca2 = u.dot(u);  // cMinusA.dot(cMinusA);
   }

   private void check() {
      System.out.println("E-A=" + eMinusA + "\n" +
              "B-A=" + r + "\n" + " C-A=" + u +  "\n" +
              " perp. dot prods: \n" +
              r.dot(r) + " " + r.dot(u) + " " + r.dot(eMinusA)+
  "\n" +             u.dot(r) + " " + u.dot(u) + " " + u.dot(eMinusA)+
  "\n" +      eMinusA.dot(r) + " " + eMinusA.dot(u) + " " + eMinusA.dot(eMinusA) );
   }

   public Triple project( Triple p ) {
 
      Triple pMinusE = p.subtract(e);

      double bape = r.dot(pMinusE);
      double cape = u.dot(pMinusE);
      double eape = eMinusA.dot(pMinusE);

      double lambda = - ea2 / eape;
      double beta = lambda * ( bape / ba2 );
      double gamma = lambda * ( cape / ca2 );

      return new Triple( beta, gamma, lambda );

   }// project
   
   // change azi by da
   public void turnBy( double da ) {
      azi += da;
      if ( azi >= 360 ) {
         azi -= 360;
      }
      if ( azi <= -360 ) {
         azi += 360;
      }
      update();
   }

   public void tiltBy( double da ) {
      alt += da;
      if ( alt > 89 )  alt = 89;
      if ( alt < -89 )  alt = -89;
      update();
   }

   public void move( double x, double y, double z ) {
      e = e.add( new Triple(x,y,z) );
      update();
   }
   public void move( Triple dv ) {
      e = e.add( dv );
      update();
   }

   public void zoomBy( double dd ) {
      d += dd;
      update();
   }

// *******************************************************************
//  methods added for RayTracing
// *******************************************************************

   // locate the uniforms belonging to camera
   public void getUniformLocations( int programHandle ) {
      eyeLoc = GL20.glGetUniformLocation( programHandle, "eye" );     
      centerLoc = GL20.glGetUniformLocation( programHandle, "center" );     
      rightLoc = GL20.glGetUniformLocation( programHandle, "right" );     
      upLoc = GL20.glGetUniformLocation( programHandle, "up" );     
   }

   public void sendUniforms() {
      eyeBuffer.rewind();
      e.sendData( eyeBuffer );
      eyeBuffer.rewind();
      GL20.glUniform3fv( eyeLoc, eyeBuffer );

      centerBuffer.rewind();
      a.sendData( centerBuffer );
      centerBuffer.rewind();
      GL20.glUniform3fv( centerLoc, centerBuffer );

      rightBuffer.rewind();
      r.sendData( rightBuffer );
      rightBuffer.rewind();
      GL20.glUniform3fv( rightLoc, rightBuffer );

      upBuffer.rewind();
      u.sendData( upBuffer );
      upBuffer.rewind();
      GL20.glUniform3fv( upLoc, upBuffer );
   }

   public String toString() {
      return "eye: " + e + " azi: " + azi + " alt: " + alt;
   }

}
