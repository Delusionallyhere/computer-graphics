import java.util.Scanner;
import java.nio.FloatBuffer;

import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;

/*
   a sphere has geometry and material
   properties
*/

public class Sphere{

  private static int nextId = 0;

  private int id;  // spheres are numbered 0, 1, ... in order of creation

  // geometry info
  private Vec4 center;
  private double radius;

  // material properties
  private int material;  // index into static list in Material class

  // uniform locations of components in fragment shader
  private int cenLoc, radLoc, emiLoc, ambiLoc, diffLoc, specLoc, shiniLoc;

  private FloatBuffer cenBuffer, emiBuffer, ambiBuffer,
                      diffBuffer, specBuffer;

  public Sphere( Scanner input ){
   
    id = nextId;    nextId++;

    center = new Vec4( input );
    radius = input.nextDouble();

    String materialName = input.next();
    material = Material.get( materialName );
System.out.println("got material " + materialName + " " + material +
  " for sphere #" + id );

  }// constructor

  public void setLocs( int programHandle ){
    cenLoc = GL20.glGetUniformLocation( programHandle,
                 "spheres[" + id + "].center" );
      Util.error( "get loc for center" );
    radLoc = GL20.glGetUniformLocation( programHandle,
                 "spheres[" + id + "].radius" );
      Util.error( "get loc for radius" );
    emiLoc = GL20.glGetUniformLocation( programHandle,
                 "spheres[" + id + "].emi" );
      Util.error( "get loc for emi" );
    ambiLoc = GL20.glGetUniformLocation( programHandle,
                 "spheres[" + id + "].ambi" );
      Util.error( "get loc for ambi" );
    diffLoc = GL20.glGetUniformLocation( programHandle,
                 "spheres[" + id + "].diff" );
      Util.error( "get loc for diff" );
    specLoc = GL20.glGetUniformLocation( programHandle,
                 "spheres[" + id + "].spec" );
      Util.error( "get loc for spec" );
    shiniLoc = GL20.glGetUniformLocation( programHandle,
                 "spheres[" + id + "].shini" );
      Util.error( "get loc for shini" );
  }

  // send data for this sphere to uniforms
  public void sendUniforms(){

    cenBuffer = center.toBuffer();
    GL20.glUniform4fv( cenLoc, cenBuffer );
      Util.error( "send center" );
   
    GL20.glUniform1f( radLoc, (float) radius );
      Util.error( "send radius" );

    emiBuffer = Material.mat.get( material ).emissive.toBuffer();
    GL20.glUniform4fv( emiLoc, emiBuffer );
      Util.error( "send emi" );

    ambiBuffer = Material.mat.get( material ).ambient.toBuffer();
    GL20.glUniform4fv( ambiLoc, ambiBuffer );
      Util.error( "send ambi" );

    diffBuffer = Material.mat.get( material ).diffuse.toBuffer();
    GL20.glUniform4fv( diffLoc, diffBuffer );
      Util.error( "send diff" );

    specBuffer = Material.mat.get( material ).specular.toBuffer();
    GL20.glUniform4fv( specLoc, specBuffer );
      Util.error( "send spec" );

    GL20.glUniform1f(shiniLoc, (float) (Material.mat.get( material ).shininess) );
      Util.error( "send shini" );

  }// sendUniforms

}// Sphere class
