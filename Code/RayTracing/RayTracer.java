/*  
  read a data file given data for a collection
  of spheres and then draw them using a
  simple ray tracer

  Use natural viewing---pass eye, center, right, up
  as uniforms to fragment shader
*/

import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

 import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
 import static org.lwjgl.system.MemoryUtil.*;

import java.util.*;
import java.io.*;

public class RayTracer extends Basic
{
  public static void main(String[] args) {
     if ( args.length != 1 ) {
        System.out.println("Need name of scene data file as command line arg");
        System.exit(1);
     }
     else {
       RayTracer app = new RayTracer( "Sphere Scene", 500, 500, 30, args[0] );
       app.start();
     }
  }// main

  // instance variables 

  private FloatBuffer backColor;
  private Shader v1, f1;
  private int hp1;
  private Program p1;

  private int vao;  // handle to the vertex array object

  // vertices simply fill the screen with two triangles
  private float[] positionData = {-1,-1, 0,
                                     1, -1, 0,
                                     1, 1, 0,
                                   -1, -1, 0,
                                    1, 1, 0,
                                    -1, 1, 0 };

  private double near;
  private ArrayList<Sphere> spheres;
  private Light light;

  private Camera camera;

  // quantities that this app passes as uniforms to fragment shader
  // (other such quantities are done by Light, Sphere instances)
  private int numSpheresLoc;
  // note:  locs for pixel grid in space stuff stored in camera
  
  public RayTracer( String appTitle, int pw, int ph, int fps, String fileName )
  {
    super( appTitle, pw, ph, (long) ((1.0/fps)*1000000000) );


    // create camera
    // (note:  pixel grid in space will be 2 by 2, which
    //         sets scale for the "dist" argument here)
    camera = new Camera( new Triple(50,-250,50), 90, 0, 8 );

    // read sphere info
    try{

     Scanner input;

     // create desired materials (must do before loading things that use
     // materials)
      input = new Scanner( new File( "standard.mat" ) );
      Material.load( input );
      input.close();

      spheres = new ArrayList<Sphere>();

      input = new Scanner( new File( fileName ) );

      int num = input.nextInt();  // # of stationary things

      for( int k=0; k<num; k++ ){
        Sphere s = new Sphere( input );
        spheres.add( s );
      }

      input.close();

    }
    catch(Exception e){
      System.out.println("something went wrong with reading file");
      e.printStackTrace();
      System.exit(1);
    }

  }

  protected void init()
  {
    String vertexShaderCode = Shader.readFile( "rayTracer.vert" );
    System.out.println("Vertex shader:\n" + vertexShaderCode + "\n\n" );

    v1 = new Shader( "vertex", vertexShaderCode );

    String fragmentShaderCode = Shader.readFile( "rayTracer.frag" );
    System.out.println("Fragment shader:\n" + fragmentShaderCode + "\n\n" );

    f1 = new Shader( "fragment", fragmentShaderCode );

    hp1 = GL20.glCreateProgram();
         Util.error("after create program");
         System.out.println("program handle is " + hp1 );

    GL20.glAttachShader( hp1, v1.getHandle() );
         Util.error("after attach vertex shader to program");

    GL20.glAttachShader( hp1, f1.getHandle() );
         Util.error("after attach fragment shader to program");

    GL20.glLinkProgram( hp1 );
         Util.error("after link program" );

    GL20.glUseProgram( hp1 );
         Util.error("after use program");

    // set background color to white
    backColor = Util.makeBuffer4( 1.0f, 1.0f, 1.0f, 1.0f );

    // create vertex buffer objects and their handles one at a time
    int positionHandle = GL15.glGenBuffers();
    System.out.println("have position handle " + positionHandle );

    // connect data to the VBO's
    // first turn the arrays into buffers
    FloatBuffer positionBuffer = Util.arrayToBuffer( positionData );

Util.showBuffer("position buffer: ", positionBuffer );  positionBuffer.rewind();

       // now connect the buffers
       GL15.glBindBuffer( GL15.GL_ARRAY_BUFFER, positionHandle );
             Util.error("after bind positionHandle");
       GL15.glBufferData( GL15.GL_ARRAY_BUFFER, 
                                     positionBuffer, GL15.GL_STATIC_DRAW );
             Util.error("after set position data");

    // set up vertex array object

      // using convenience form that produces one vertex array handle
      vao = GL30.glGenVertexArrays();
           Util.error("after generate single vertex array");
      GL30.glBindVertexArray( vao );
           Util.error("after bind the vao");
      System.out.println("vao is " + vao );

      // enable the vertex array attributes
      GL20.glEnableVertexAttribArray(0);  // position
             Util.error("after enable attrib 0");
  
      // map index 0 to the position buffer 
      GL15.glBindBuffer( GL15.GL_ARRAY_BUFFER, positionHandle );
             Util.error("after bind position buffer");
      GL20.glVertexAttribPointer( 0, 3, GL11.GL_FLOAT, false, 0, 0 );
             Util.error("after do position vertex attrib pointer");

    // create light and locate its uniforms:
    light = new Light( hp1, new Vec4( 25, 25, 100 ),  // position of light
                       new Vec4( 1.0, 1.0, 1, 1 ),    // ambi
                       new Vec4( 1, 1, 1, 1 ),    // diff
                       new Vec4( 1, 1, 1, 1 ) );  // spec

    // locate uniforms owned by this app:
    numSpheresLoc = GL20.glGetUniformLocation( hp1, "numSpheres" );
       System.out.println("numspheres = " + numSpheresLoc );

    camera.getUniformLocations( hp1 );

    // locate uniforms for all the spheres
    for( int k=0; k<spheres.size(); k++ ){
      spheres.get(k).setLocs( hp1 );
    }

  }

  protected void processInputs()
  {
    // process all waiting input events
    while( InputInfo.size() > 0 )
    {
      InputInfo info = InputInfo.get();

      if( info.kind == 'k' && (info.action == GLFW_PRESS || 
                               info.action == GLFW_REPEAT) )
      {
        int code = info.code;

        // interactively move the camera around
        // enjoying the scene

        if( code == GLFW_KEY_X && (info.mods & 1) == 0 ){// x for left
          camera.move( -1, 0, 0 );
        }
        else if( code == GLFW_KEY_X && (info.mods & 1) == 1 ){// X for right
          camera.move( 1, 0, 0 );
        }

        else if( code == GLFW_KEY_Y && (info.mods & 1) == 0 ){// y for near
          camera.move( 0, -1, 0 );
        }
        else if( code == GLFW_KEY_Y && (info.mods & 1) == 1 ){// Y for far
          camera.move( 0, 1, 0 );
        }

        else if( code == GLFW_KEY_Z && (info.mods & 1) == 0 ){// z for down
          camera.move( 0,0,-1 );
        }
        else if( code == GLFW_KEY_Z && (info.mods & 1) == 1 ){// Z for up
          camera.move( 0,0,1 );
        }

        else if( code == GLFW_KEY_R && (info.mods & 1) == 0 ){// r to rotate
           camera.turnBy( 3 );
        }
        else if( code == GLFW_KEY_R && (info.mods & 1) == 1 ){// R to rotate
           camera.turnBy( -3 );
        }

        else if( code == GLFW_KEY_T && (info.mods & 1) == 0 ){// t to tilt
           camera.tiltBy( 1 );
        }
        else if( code == GLFW_KEY_T && (info.mods & 1) == 1 ){// T to tilt
           camera.tiltBy( -1 );
        }

        System.out.println("camera data: " + camera );

      }// input event is a key

      else if ( info.kind == 'm' )
      {// mouse moved
      //  System.out.println( info );
      }

      else if( info.kind == 'b' )
      {// button action
       //  System.out.println( info );
      }

    }// loop to process all input events

  }

  protected void update()
  {
  }

  protected void display()
  {
    GL30.glClearBufferfv( GL11.GL_COLOR, 0, backColor );

    // send uniforms owned by this app:
    GL20.glUniform1i( numSpheresLoc, spheres.size() );

    camera.sendUniforms();
   
    // send uniforms for light and spheres to fragment shader:
    light.sendUniforms();

    for( int k=0; k<spheres.size(); k++ ){
      spheres.get(k).sendUniforms();
    }

    // activate vao
    GL30.glBindVertexArray( vao );
           Util.error("after bind vao");

    // draw the buffers
    GL11.glDrawArrays( GL11.GL_TRIANGLES, 0, 6 );
           Util.error("after draw arrays");

  }

}// RayTracer
