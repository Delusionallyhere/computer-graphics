public class Camera {

   private Triple e, a;
   private Triple eMinusA;
   private Triple r;  // B-A
   private Triple u;  // C-A

   private double azi, alt;
   private double d;  // distance from E to A

   private double ea2, ba2, ca2;

   // given eye point, azimuth and altitude (in degrees)
   // and distance from eye to center of plexiglass
   // construct a camera
   public Camera( Triple eye, double azimuth, 
                  double altitude, double dist ) {
      e = eye;
      azi = azimuth;  alt = altitude;
      d = dist;
      
      update();

   }// constructor

   // recompute instance fields when one of them
   // changes (azi, alt, d, e)
   private void update() {
   
      // see page 33
      double alpha = Math.toRadians(azi);
      double beta = Math.toRadians(alt);
      double ca = Math.cos(alpha);
      double sa = Math.sin(alpha);
      double cb = Math.cos(beta);
      double sb = Math.sin(beta);
    
      a = new Triple( e.x + cb*d*ca,
                      e.y + cb*d*sa,
                      e.z + d*sb );

      eMinusA = e.subtract(a);
      r = Triple.zAxis.cross( eMinusA ).normalize();
      u = eMinusA.cross(r).normalize();
      // B = A +r, C = A + u

      ea2 = eMinusA.dot(eMinusA);
      ba2 = r.dot(r);  // bMinusA.dot(bMinusA);
      ca2 = u.dot(u);  // cMinusA.dot(cMinusA);
   }

   private void check() {
      System.out.println("E-A=" + eMinusA + "\n" +
              "B-A=" + r + "\n" + " C-A=" + u +  "\n" +
              " perp. dot prods: " +
              r.dot(r) + " " + r.dot(u) + " " + r.dot(eMinusA)+
  "\n" +             u.dot(r) + " " + u.dot(u) + " " + u.dot(eMinusA)+
         "\n" +      eMinusA.dot(r) + " " + eMinusA.dot(u) + " " + 
             "\n" +  eMinusA.dot(eMinusA) );
   }

   public Triple project( Triple p ) {
 
      Triple pMinusE = p.subtract(e);

      double bape = r.dot(pMinusE);
      double cape = u.dot(pMinusE);
      double eape = eMinusA.dot(pMinusE);

      double lambda = - ea2 / eape;
      double beta = lambda * ( bape / ba2 );
      double gamma = lambda * ( cape / ca2 );

      return new Triple( beta, gamma, lambda );

   }// project
   
   // change azi by da
   public void turnBy( double da ) {
      azi += da;
      update();
   }

   public void tiltBy( double da ) {
      alt += da;
      // oops!
      update();
   }

   public void shiftBy( Triple dv ) {
      e = e.add( dv );
      update();
   }

   public void zoomBy( double dd ) {
      d += dd;
      // oops!
      update();
   }

   public static void main(String[] args) {
      java.util.Scanner keys = 
           new java.util.Scanner( System.in );
      System.out.print("enter eye point: ");
      Triple e = new Triple( keys );
      System.out.print("enter azimuth (in degrees): ");
      double azi = keys.nextDouble();
      System.out.print("enter altitude (in degrees): ");
      double alt = keys.nextDouble();
      System.out.print("enter distance from eye to center: ");
      double dist = keys.nextDouble();

      Camera cam = new Camera( e, azi, alt, dist );
      cam.check();

      System.out.print("enter point to project: ");
      Triple p = new Triple( keys );
      Triple q = cam.project( p );
      System.out.println("projected point is " + q );
    
   }

}
