import java.nio.FloatBuffer;

public class Box {

    double x, y, w, h, speed, bearing, ammo;
    String kind;

    //TODO: same as below
    public Box(Triangle tri1, Triangle tri2, String string) {
        w = Math.abs((tri1.a.x - tri1.b.x) / 2);
        h = Math.abs((tri1.b.y - tri1.c.y) / 2);
        x = tri1.a.p.x - w;
        y = tri1.a.p.y + h;
        kind = string;
    }

    //TODO: update from triangles to 4 vertices, creating triangles out of the vertices to draw.
    public Box(Triangle tri1, Triangle tri2, String string, double speed, double bearing, double ammo) {
        w = Math.abs((tri1.a.p.x - tri1.b.p.x) / 2);
        h = Math.abs((tri1.b.p.y - tri1.c.p.y) / 2);
        x = tri1.a.p.x - w;
        y = tri1.a.p.y + h;
        kind = string;
        speed = speed;
        bearing = bearing;
        ammo = ammo;
    }

    //TODO: update the if statements to correct terms
    public boolean collision(Box box1, Box[] box2) {
        for (int i = 0; i < box2.length; i++) {
            if (box1.tri1.b.p.x > box2[i].tri1.a.p.x || box1.tri1.c.p.x > box2[i].tri2.b.p.x) {
                if (box1.tri2.a.p.y > box2[i].tri1.b.p.y || box1.tri2.b.p.y > box2[i].tri1.a.p.y) {
                    if (box1.tri1.a.p.y > box2[i].tri2.b.p.y || box1.tri1.b.p.y > box2[i]. tri2.a.p.y) {
                        switch(box1.kind) {
                            case "bullet":
                                switch(box2[i].kind) {
                                    case "ammunitionBox":
                                        return false;
                                    case "blueTank":
                                        box2.remove(i);
                                        System.out.println("Red Tank has won the game!\n");
                                        System.exit(1);
                                    case "bullet":
                                        //TODO: remove first bullet
                                        box2.remove(i);
                                        return true;
                                    case "mine":
                                        //TODO: remove bullet
                                        box2.remove(i);
                                        return true;
                                    case "redTank":
                                        box2.remove(i);
                                        System.out.println("Blue Tank has won the game!\n");
                                        System.exit(1);
                                    case "wall":
                                        //TODO: remove bullet
                                        return false;
                                }
                                break;
                            case "blueTank":
                                switch(box2[i].kind) {
                                    case "ammuntionBox":
                                        box1.ammo += 5;
                                        box2.remove(i);
                                        return false;
                                    case "bullet":
                                        System.out.println("Red Tank has won the game!\n");
                                        System.exit(1);
                                    case "mine":
                                        System.out.println("Red Tank has won the game!\n");
                                        System.exit(1);
                                    case "redTank":
                                        box1.speed = 0;
                                        box2[i].speed = 0;
                                        return true;
                                    case "wall":
                                        box1.speed = 0;
                                        return true;
                                }
                                break;
                            case "redTank":
                                switch(box2[i].kind) {
                                    case "ammunitionBox":
                                        box1.ammo += 5;
                                        box2.remove(i);
                                        return false;
                                    case "blueTank":
                                        box1.speed = 0;
                                        box2[i].speed = 0;
                                        return true;
                                    case "bullet":
                                        System.out.println("Blue Tank has won the game!\n");
                                        System.exit(1);
                                    case "mine":
                                        System.out.println("Blue Tank has won the game!\n");
                                        System.exit(1);
                                    case "wall":
                                        box1.speed = 0;
                                        return true;
                                }
                                break;
                        }
                    }
                    else { return false; }
                }
            }
        }
    }

    public void sendData(FloatBuffer pb, FloatBuffer cb) {
        x.sendData(pb, cb);
        y.sendData(pb, cb);
        w.sendData(pb, cb);
        h.sendData(pb, cb);
    }
}
