/*
   "natural viewing camera":
   has E, A, B, C generated
   from parameters
*/

import java.util.Scanner;

public class Camera {

   private Triple e;
   private double azi;  // rotation of camera in x-y plane
   private double alt;  // rotation of camera from x-y plane
   private double d;  // distance from e to a

   private Triple eMinusA, bMinusA, cMinusA;
   private double ea2;  // ba2 and ca2 should be 1

   public Camera( Triple eye, double azimuth, double altitude, double dist ) {
      compute( eye, azimuth, altitude, dist );
   }

   private void compute( Triple eye, double azimuth, double altitude, double dist ) {
      e = eye;
      azi = azimuth;
      alt = altitude;
      d = dist;

      double alpha = Math.toRadians( azi );
      double beta = Math.toRadians( alt );

      double cosBeta = Math.cos( beta );
      double sinBeta = Math.sin( beta );
      double cosAlpha = Math.cos( alpha );
      double sinAlpha = Math.sin( alpha );

      Triple a = new Triple( e.x + cosBeta * d * cosAlpha,
                      e.y + cosBeta * d * sinAlpha,
                      e.z + d * sinBeta
                    );

      eMinusA = e.minus( a );
      bMinusA = Triple.up.cross( eMinusA ).normalize();
      cMinusA = eMinusA.cross( bMinusA ).normalize();

      ea2 = eMinusA.dot( eMinusA );
   }

   public void turn( double dazi ) {
      azi += dazi;
      compute( e, azi, alt, d );
   }

   public void tilt( double dalt ) {
      alt += dalt;
      compute( e, azi, alt, d );
   }

   public void zoom( double v ) {
      d += v;
      compute( e, azi, alt, d );
   }

   public void move( double dx, double dy, double dz ) {
      e = new Triple( e.x + dx, e.y + dy, e.z + dz );
      compute( e, azi, alt, d );
   }

   public Camera( Scanner input ) {
      Triple eye = new Triple( input );
      double azimuth = input.nextDouble();
      double altitude = input.nextDouble();
      double dist = input.nextDouble();
      input.nextLine();
      input.nextLine();

      compute( eye, azimuth, altitude, dist );
   }

   // from the point p in space, get the
   // perspective transformed (beta, gamma, lambda )
   public Triple viewTransform( Triple p ) {
      Triple pMinusE = p.minus( e );
      double lambda = - ea2 / eMinusA.dot( pMinusE );
      double beta = lambda * bMinusA.dot( pMinusE );  // b-a has length 1
      double gamma = lambda * cMinusA.dot( pMinusE );  // c-a has length 1

      return new Triple( beta, gamma, lambda );
   }

}
